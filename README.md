Laragon
	git clone https://bitbucket.org/jmdgoncalves/projeto2020.git
	composer install (requires PHP 7.3+)
	php artisan key:generate
	php artisan migrate
	php artisan db:seed
	npm install
	npm run watch (-poll if it fails)