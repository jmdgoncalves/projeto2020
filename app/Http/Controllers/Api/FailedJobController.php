<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FailedJob;

class FailedJobController extends Controller
{
    public function get()
    {
        return FailedJob::all();
    }
}
