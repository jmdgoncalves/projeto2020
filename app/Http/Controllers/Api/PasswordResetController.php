<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PasswordReset;

class PasswordResetController extends Controller
{
    public function get()
    {
        return PasswordReset::all();
        //
    }
}
