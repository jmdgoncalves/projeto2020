<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product = new Product();
        $product->fill($request->validated());

        $product->created_at = now();
        $product->updated_at = now();

        $photo = $request->file('photo_url');
        $fileName = time() . '.' . $photo->getClientOriginalExtension();
        $product->photo_url = $fileName;
        Storage::disk('public')->put('products/' . $fileName, file_get_contents($photo));

        $product->save();
        return response()->json($product, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
       
        $product->fill($request->validated());
        $product->updated_at = now();
        if ($request->file('photo_url') != null) {
            $photo = $request->file('photo_url');
            $fileName = time() . '.' . $photo->getClientOriginalExtension();
            $product->photo_url = $fileName;
            Storage::disk('public')->put('products/' . $fileName, file_get_contents($photo));
        }
        
        $product->save();
        return response()->json($product, 201);
    }

    public function restore(int $id)
    {
        $product = Product::withTrashed()->findOrFail($id);
        $product->restore();
        return response()->json($product, 201);
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json($product, 204);
    }

    public function get()
    {
        return Product::all();
        //
    }
}
