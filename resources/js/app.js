require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import App from './views/App'
import Welcome from './views/Welcome'
import Users from './views/Users'
import Menu from './views/Menu'
import LoginComponent from './components/login'
import ErrorComponent from './components/error'
import ImagePickerComponent from './components/image-picker'
import store from "./stores/global-store"

Vue.component('login',LoginComponent)
Vue.component('error',ErrorComponent)
Vue.component('image-Picker',ImagePickerComponent)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Welcome
        }, {
            path: '/users',
            name: 'users',
            component: Users
        }, {
            path: '/menu',
            name: 'menu',
            component: Menu,
            props: true
        },
    ],
});
const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store,
});
