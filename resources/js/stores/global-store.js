import Vue from "vue"
import Vuex from "vuex"
Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        loggedUser: false,
    },
    mutations: {
        clearUser(state) {
            state.loggedUser = false
        },
        setUser(state, user) {
            state.loggedUser = user
        },
    }
})