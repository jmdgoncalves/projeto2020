<!DOCTYPE html>
<html lang="en">
<head>

<link href=" {{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div id="app">
		<app></app>
	</div>
	<script src="js/app.js"></script>
</body>
</html>