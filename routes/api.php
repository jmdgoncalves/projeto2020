<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\FailedJobController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\OrderItemController;
use App\Http\Controllers\Api\PasswordResetController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', [UserController::class, 'login']);
Route::middleware('auth:sanctum')->post(
    'logout',
    [UserController::class, 'logout']
);

Route::middleware('auth:sanctum')->get(
    'users',
    [UserController::class, 'get']
);
Route::middleware('auth:sanctum')->post(
    'users/{user}/available',
    [UserController::class, 'updateAvailability']
);
Route::middleware('auth:sanctum')->post(
    'users/{user}/logged',
    [UserController::class, 'updateLoggedIn']
);
Route::middleware('auth:sanctum')->get('users/me', [UserController::class, 'me']);
//Route::get('users',[UserController::class, 'get']);

Route::get('customers', [CustomerController::class, 'get']);
Route::get('products', [ProductController::class, 'get']);
Route::post('products', [ProductController::class, 'store']);
Route::delete('products/{product}',       [ProductController::class, 'destroy']);
Route::post('products/{product}',       [ProductController::class, 'update']); // Laravel can not handle multipart-formdata well with PUT method. See Input from PUT requests sent as multipart/form-data is unavailable #13457.
Route::put('products/restore/{id}',       [ProductController::class, 'restore']);

//Route::get('failedjobs',[FailedJobController::class, 'get']);

//Route::get('orders',[OrderController::class, 'get']);
//Route::get('orderitems',[OrderItemController::class, 'get']);

//Route::get('passwordresets',[PasswordResetController::class, 'get']);


